/**
 * The beginning of a unit test for MineSweeper.  
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Array;

import org.junit.Test;

public class MineSweeperTest {

	@Test
	public void testGetAdjacentMinesWithAGivenTwodArrayOfBooleans() {

		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		// Use the non-random constructor when testing to avoid random mine
		// placement.
		MineSweeper ms = new MineSweeper(b1);

		// Check adjacent mines around every possible GameSquare
		assertEquals(0, ms.getAdjacentMines(0, 0));
		assertEquals(1, ms.getAdjacentMines(0, 1));
		assertEquals(2, ms.getAdjacentMines(0, 2));
		assertEquals(2, ms.getAdjacentMines(0, 3));
		assertEquals(1, ms.getAdjacentMines(0, 4));

		assertEquals(0, ms.getAdjacentMines(1, 0));
		assertEquals(1, ms.getAdjacentMines(1, 1));
		assertEquals(2, ms.getAdjacentMines(1, 2)); // works even if it is a
													// mine
		assertEquals(2, ms.getAdjacentMines(1, 3));
		assertEquals(2, ms.getAdjacentMines(1, 4));

		assertEquals(0, ms.getAdjacentMines(2, 0));
		assertEquals(1, ms.getAdjacentMines(2, 1));
		assertEquals(3, ms.getAdjacentMines(2, 2));
		assertEquals(2, ms.getAdjacentMines(2, 3));
		assertEquals(2, ms.getAdjacentMines(2, 4));
	}

	@Test
	public void testGetTotalMineCount() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);
		assertEquals(3, ms.getTotalMineCount());

		b1 = new boolean[1][0];
		ms = new MineSweeper(b1);
		assertEquals(0, ms.getTotalMineCount());

		b1 = new boolean[1][1];
		b1[0][0] = true;
		ms = new MineSweeper(b1);
		assertEquals(1, ms.getTotalMineCount());
	}

	@Test
	public void testIsFlagged() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);

		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				ms.toggleFlagged(row, col);
			}
		}

		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				if (!b1[row][col]) {
					assertTrue(ms.isFlagged(row, col));
				}
			}
		}

	}

	@Test
	public void testIsMine() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);
		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				if (b1[row][col]) {
					assertTrue(ms.isMine(row, col));
				} else {
					assertFalse(ms.isMine(row, col));
				}
			}
		}
	}

	@Test
	public void testIsVisible() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);
		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				assertFalse(ms.isVisible(row, col));
			}
		}

		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				if (!ms.isMine(row, col)) {
					ms.click(row, col);
					assertTrue(ms.isVisible(row, col));
				}
			}
		}

	}

	@Test
	public void testLost() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);
		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				if (ms.isMine(row, col)) {
					ms.click(row, col);
					assertTrue(ms.lost());
					break;
				}
			}
		}
	}

	@Test
	public void testWon() {
		boolean[][] b1 =

		{ { false, false, false, false, false },
				{ false, false, true, true, false },
				{ false, false, false, true, false }, };

		MineSweeper ms = new MineSweeper(b1);
		for (int row = 0; row < b1.length; row++) {
			for (int col = 0; col < b1[0].length; col++) {
				if (!ms.isMine(row, col)) {
					ms.click(row, col);
				}
			}
		}
		
		assertTrue(!ms.lost());
		assertTrue(ms.won());
	}
}