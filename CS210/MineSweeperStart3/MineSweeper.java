import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

/**
 * This class represents the model for a game of MineSweeper. It has a
 * constructor that takes a preset boolean 2D array where true means there is a
 * mine. This first constructor (you'll need 2) is for testing the methods of
 * this class.
 * 
 * The second constructor that takes the number of rows, the number of columns,
 * and the number of mines to be set randomly in that sized mine field. Do this
 * last.
 * 
 * @author YOUR NAME
 */
public class MineSweeper implements MineSweeperModel {

	private class GameSquare {

		private boolean isMine;
		private int row;
		private int col;
		private boolean isVisible;
		private boolean isFlagged;
		private int mineNeighbors;

		// Construct a GameSquare object with all values initialized except
		// mineNeighbors, which is an instance variables that can only be set
		// after
		// all
		// GameSquare objects have been constructed in the 2D array.
		public GameSquare(boolean isMine, int row, int col) {
			this.isMine = isMine;
			this.row = row;
			this.col = col;
			isVisible = false; // Default until someone starts clicking
			isFlagged = false; // Default until someone starts clicking
			// call setAdjacentMines() from both constructors
			// to set this for each new GameSquare.
			mineNeighbors = 0;
		}
	}

	// The instance variable represents all GameSquare objects where each knows
	// its row,
	// column, number of mines around it, if it is a mine, flagged, or visible
	private GameSquare[][] board;
	
	private boolean lost = false;

	/**
	 * Construct a MineSweeper object using a given mine field represented by an
	 * array of boolean values: true means there is mine, false means there is
	 * not a mine at that location.
	 * 
	 * @param mines
	 *            A 2D array to represent a mine field so all methods can be
	 *            tested with no random placements.
	 */
	public MineSweeper(boolean[][] mines) {
		sharedConstructorCode(mines);
	}

	private void sharedConstructorCode(boolean[][] mines) {
		// TODO: Complete this constructor first so you can test preset mine
		// fields
		// (later on you will need to write another constructor for random
		// boards).
		// new GameSquare objects store all info about one square on the board
		// such
		// as its row, column, if it's flagged, visible, or is a mine.

		board = new GameSquare[mines.length][mines[0].length];

		// Example construction of one GameSquare stored in row 2, column 4:
		// /// board[2][4] = new GameSquare(mines[2][4], 2, 4);
		// Use a nested for loop to change all board array elements
		// from null to a new GameSquare
		for (int row = 0; row < mines.length; row++) {
			for (int col = 0; col < mines[0].length; col++) {
				board[row][col] = new GameSquare(mines[row][col], row, col);
			}
		}

		// You will need to call private void setAdjacentMines() to set
		// mineNeighbors for all GameSquare objects because each GameSquare
		// object
		// must first know if it is a mine or not. Set mineNeighbors for each.
		setAdjacentMines();
	}

	/**
	 * Use the almost initialized 2D array of GameSquare objects to set the
	 * instance variable mineNeighbors for every 2D array element (even if that
	 * one GameSquare has a mine). This is similar to GameOfLife neighborCount.
	 */
	private void setAdjacentMines() {
		// Example to set the instance variable mineNeighbors of the one
		// GameSquare
		// object stored in row 2, column 4 to 8:
		// /// board[2][4].mineNeighbors = 8;
		// Use a nested for loop to set mineNeighbors for ALL GameSquare objects
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				board[row][col].mineNeighbors = this.calculateAdjacentMines(row, col);
			}
		}
	}
	
	

	/**
	 * This method returns the number of mines surrounding the requested
	 * GameSquare (the mineNeighbors value of the square). A square with a mine
	 * may return the number of surrounding mines, even though it will never
	 * display that information.
	 * 
	 * @param row
	 *            - An int value representing the row in board.
	 * @param column
	 *            - An int value representing the column in board.
	 * @return The number of mines surrounding to this GameSquare
	 *         (mineNeighbors)
	 * 
	 *         Note: This should run O(1) because setAdjacentMines should
	 *         already have set the instance variable mineNeighbors in every
	 *         GameSquare object
	 */
	public int getAdjacentMines(int row, int column) {
		return board[row][column].mineNeighbors;
	}

	private int calculateAdjacentMines(int row, int column) {
		int minesTally = 0;
		for (GameSquare neighbor : getNeighbors(row, column)) {
			minesTally += neighbor.isMine ? 1 : 0;
		}
		return minesTally;
	}

	private List<GameSquare> getNeighbors(int row, int column) {
		int nextRow = row + 1;
		int previousRow = row - 1;
		int nextColumn = column + 1;
		int previousColumn = column - 1;

		//
		// Now that you have the nextRow / prevRow, how do you represent all the
		// possible neighbors?
		//
		int neighbors[][] = { { previousRow, previousColumn },
				{ previousRow, column }, { previousRow, nextColumn },
				{ row, previousColumn }, { row, nextColumn },
				{ nextRow, previousColumn }, { nextRow, column },
				{ nextRow, nextColumn } };

		List<GameSquare> sqs = new ArrayList<GameSquare>();
		for (int [] neighbor : neighbors) {
			if (neighbor[0] >= 0 && neighbor[0] < board.length &&
					neighbor[1] >= 0 && neighbor[1] < board[0].length) {
				sqs.add(board[neighbor[0]][neighbor[1]]);
			}
		}
		return sqs;
	}

	/**
	 * Construct a MineSweeper of any size that has numberOfMines randomly set
	 * so we can get a different unexpected game.
	 * 
	 * @param rows
	 *            Height of the board
	 * @param columns
	 *            Width of the board
	 * @param numberOfMines
	 *            How m any mines are to randomly placed
	 */
	public MineSweeper(int rows, int columns, int numberOfMines) {
		boolean[][] tempLayout = new boolean[rows][columns];
		for(int row = 0; row < rows; row++) {
			for(int col = 0; col < columns; col++) {
				tempLayout[row][col] = false;
			}
		}

		Random generator = new Random();
		
		for(int i = 0; i < numberOfMines; i++) {
			int r = -1;
			int c = -1;
			do {
				r = generator.nextInt(rows);
				c = generator.nextInt(columns);
			} while (tempLayout[r][c] == true);
			
			assert (r >= 0 && r < rows && c >= 0 && c < columns);
			tempLayout[r][c] = true;
		}
		sharedConstructorCode(tempLayout);
	}

	/**
	 * This method returns the number of mines found in the game board.
	 * 
	 * @return The number of mines.
	 */
	public int getTotalMineCount() {
		int mineTally = 0;
		for(int row = 0; row < board.length; row++) {
			for(int col = 0; col < board[0].length; col++) {
				mineTally += board[row][col].isMine ? 1 : 0;
			}
		}
		return mineTally;
	}

	/**
	 * This method returned whether or not the square has been flagged by the
	 * user. Flags are a tool used by players to quickly tell which squares they
	 * think contain mines as well as prevent accidental clicking on those
	 * squares.
	 * 
	 * @param row
	 *            - An int value representing the row (x) value in the game
	 *            board.
	 * @param column
	 *            - An int value representing the column (y) value in the game
	 *            board.
	 * @return A boolean value representing the flagged state of this location.
	 */
	public boolean isFlagged(int row, int column) {
		return board[row][column].isFlagged;
	}

	/**
	 * This method changes the isFlagged state of a GameSquare object. If true,
	 * isFlagged becomes false. If false, isFlagged becomes true. This method
	 * will be called by the GUI when someone right-clicks a square
	 */
	public void toggleFlagged(int row, int column) {
		board[row][column].isFlagged = !board[row][column].isFlagged;
	}

	/**
	 * This method determines if the square in question is a mine.
	 * 
	 * @param row
	 *            - An int value representing the row (x) value in the game
	 *            board.
	 * @param column
	 *            - An int value representing the column (y) value in the game
	 *            board.
	 * @return A boolean representing the mine status of the square.
	 */
	public boolean isMine(int row, int column) {
		return board[row][column].isMine;
	}

	/**
	 * This method gets the visibility of the square in question. Visibilty is
	 * initially defined for all squares to be false and uncovered when the
	 * click method checks the square.
	 * 
	 * @param row
	 *            - An int value representing the row (x) value in the game
	 *            board.
	 * @param column
	 *            - An int value representing the column (y) value in the game
	 *            board.
	 * @return A boolean representing whether or not the square is set to be
	 *         visible.
	 */
	public boolean isVisible(int row, int column) {
		return board[row][column].isVisible;
	}

	/**
	 * This method determines if the player has lost on the current board. A
	 * player loses if and only if they have clicked on a mine.
	 * 
	 * @return A boolean representing player failure.
	 */
	public boolean lost() {
		return lost;
	}

	/**
	 * Returns a textual representation of the GameBoard. Squares will be
	 * represented by one character followed by a space, except the last
	 * character which will have no following space. Visible squares will either
	 * be the number of mines next to the square, a blank space if no mines are
	 * adjacent, or a '*' character for a mine. Newlines will separate each row
	 * of the game board.
	 * 
	 * @return A String representation of the game board data.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
			
		for(int row = 0; row < board.length; row++) {
			for(int col = 0; col < board[0].length; col++) {
				GameSquare gs = board[row][col]; 
				if (gs.isMine) {
					s.append('*');
				} else if (gs.mineNeighbors == 0) {
					s.append(' ');
				} else {
					s.append(gs.mineNeighbors + '0');
				}
			}
			s.append('\n');
		}
		return s.toString();
	}

	/**
	 * This method determines if a player has won the game. Winning means all
	 * non-mine squares are visible and no mines have been detonated.
	 * 
	 * @return A boolean representing whether or not the player has won.
	 */
	public boolean won() {
		boolean won = true;
		for (int row = 0; row < board.length; row++) {
			for(int col = 0; col < board[1].length; col++) {
				if (!board[row][col].isMine) {
					if (!board[row][col].isVisible) {
						won = false;
						break;
					}
				}
			}
		}
		return won;
	}

	/**
	 * This method alerts the Game Board the user has clicked on the square at
	 * the given row/column. There are five possibilities for updating the board
	 * during the click messages to your MineSweeper. The GameSquare object
	 * stored at the just clicked row and column
	 * 
	 * 1. is a mine (player looses)
	 * 
	 * 2. is visible already (do nothing)
	 * 
	 * 3. is flagged (do nothing)
	 * 
	 * 4. has mineNeighbors >- 1 (simply mark that visible)
	 * 
	 * 5. is not adjacent to any mines with mineNeighbors == 0 (mark many
	 * visible)
	 * 
	 * Because MineSweeper automatically clears all squares adjacent to any
	 * blank square connected to the square clicked, a special algorithm is
	 * needed to set the proper part of the board visible. This pseudo-code
	 * shows the suggested algorithm.
	 */
	// Check special cases first, there may be nothing to do or the user clicked
	// a mine
	// if the clicked GameSquare is flagged
	//   return, there is nothing to do
	// else if the clicked GameSquare is a mine
	//   record loss so when this click is done, lost() returns true
	// else if the clicked GameSquare has already been marked as visible
	//   return, there is nothing to do
	// else if the clicked GameSquare has 1 or more neighboring mines
	//   set the square to be visible, which applies only when mineNeighbors is
	//   1..8
	// else
	//   mark the clicked GameSquare as visible
	//   push the GameSquare onto the stack
	//   while the stack is not empty:
	//     pop the stack and mark GameSquare as the current GameSquare
	//     if the current square must has no neighboring mines (not 1..8)
	//       for each adjacent square
	//         if it's visible or flagged
	//           ignore it
	//         else
	//           push adjacent GameSquare on stack
	//           set adjacent GameSquare to visible
	/**
	 * @param row
	 *            - An int value representing the row (x) value in the game
	 *            board.
	 * @param column
	 *            - An int value representing the column (y) value in the game
	 *            board.
	 */
	public void click(int row, int column) {
		// TODO: Implement this method after most all other methods
		GameSquare gs = board[row][column];
		if (gs.isFlagged) {
			return;
		} else if (gs.isMine) {
			this.lost = true;
		} else if (gs.isVisible) {
			return;
		} else if (gs.mineNeighbors >= 1 && gs.mineNeighbors <= 8) {
			gs.isVisible = true;
		} else {
			gs.isVisible = true;
			Stack<GameSquare> stack = new Stack<GameSquare>();
			stack.push(gs);
			while(! stack.isEmpty()) {
				GameSquare curr_gs = stack.pop();
				if (curr_gs.mineNeighbors == 0) {
					for (GameSquare neighbor : this.getNeighbors(curr_gs.row, curr_gs.col)) {
						if (neighbor.isVisible || neighbor.isFlagged) {
							;
						} else {
							stack.push(neighbor);
							neighbor.isVisible = true;
						}
					}
				}
			}
		}
	}
}