/* Camilo Jacomet
 * CS210 - Iverson
 * 1/20/2017
 * Reasons for doing this: Chapter 2 Quiz
 */
public class Chapter2 {

	public static void main(String[] args) {
		 triangle(); 
	     fibonacci(); 
	}
	// Prints out triangle pointing down
	public static void triangle(){
		// Counts down lines
		for(int i = 0; i < 10; i++){
			// Spacing prior to the *
			for(int j = 0; j < i+5; j++){
				System.out.print(" ");
			}
			// Triangle *
			for(int k = 0; k < 19-2*i; k++){
				System.out.print("*");
			}
			System.out.println();
		}

	}
	
	// Prints out fibonacci sequence
	public static void fibonacci(){
		int a = 1;
		int b = 1;
		System.out.print("  "+ a + " " + b + " ");
		for(int i = 0; i < 10; i++){
			int c = b + a;
			System.out.print(c + " ");
			// Swap values for next iteration
			a = b;
			b = c;
		}

	}

}