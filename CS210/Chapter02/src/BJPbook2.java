/* Camilo Jacomet
 * CS210 - Iverson
 * 1/5/2017
 * Reasons for doing this: Assignment 1. As a result I was able to understand how to break down
 * problems more thoroughly through structural decomposition, and identifying how to structure 
 * my for loops to do its own part. 
 */
public class BJPbook2 {

	public static final int LINES = 15;
	
	public static void main(String[] args) {
		topBook();
		bjpBox();
	}
	
	// This is method is to have the appropriate amount of spacing on each side of the "Building Java Programs" text 
	// between the text and the '|' due to their being 22 characters in "Building Java Programs" I did a check to see
	// if the LINES value entered was even or odd such that the spacing can be done accordingly.
	public static void bjpText() {
		String bjp = "Building Java Programs";
		
		System.out.print('|');
		for (int i = 0; i < ((LINES * 2 + 10) - bjp.length()) / 2; i++) {
			System.out.print(' ');
		}
		System.out.print(bjp);
		
		for (int j = 0; j < ((LINES * 2 + 10) - bjp.length()) / 2 ; j++) {
				System.out.print(' ');
		}
		System.out.print('|');
	}
	
	// This method displays the "|   Building Java Programs   |" to the screen the appropriate number of times
	public static void bjpBox() {
		plusLine();
		slashLine();
		
		for (int i = 0; i < LINES / 2; i++) {
			bjpText();
			for (int j = 0; j < (i * -2) + LINES; j++) {
				System.out.print('/');
			}
			System.out.println();
		}
		plusLine();
		System.out.println();
	}
	
	//This method draws the line with '+' Symbols on both ends 
	public static void plusLine() {
		System.out.print('+');
		for (int i = 0; i < LINES * 2 + 10; i++) {
			System.out.print('-');
		}
		System.out.print('+');
	}
	
	// This method is exclusively for drawing the line of slashes after the plusLine() at the
	// top of bjpBox()
	public static void slashLine(){
		for (int h = 0; h < LINES+1; h++) {
			System.out.print('/');
		}
		System.out.println();
	}
	
	// This method is used to display the top half of the book before the "Building Java Programs" text is displayed
	public static void topBook() {
		// For loop to print the spaces prior to the plusLine()
		for (int h = 0; h < LINES ; h++) {
			System.out.print(' ');
		}
		plusLine();
		System.out.println();
		
		// The first for loop prints the appropriate amount of spaces before the '/' on the left hand side
		for (int i = 0; i < (LINES/2)*2; i++) {
			for (int j = 0; j < (i * -1) + (LINES/2)*2; j++) {
				System.out.print(' ');
			}
			System.out.print('/');
			
			// This for loop prints the spacing to the right of the first '/' before reaching the '__/' character
			for (int k = 0; k < (i * -1) + (LINES - 1); k++) {
				System.out.print("   ");
			}
			//System.out.print("_");
			
			// This for loop is used to print the appropriate amount of "__/" according 
			// to what line its on based off the initial for loop with counter i
			for (int l = 0; l < i +1 ; l++) {
				System.out.print("__/");
			}
			
			// This for loop is used to print the appropriate amount of '/' after the "__/"
			// depending on the count.
			for (int m = 0; m < i ; m++) {
				System.out.print("/");

			}
			System.out.println();

		}

	}

}
