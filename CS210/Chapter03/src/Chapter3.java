/* Camilo Jacomet
 * CS210 - Iverson
 * 1/27/2017
 * Reasons for doing this: Chapter 3 Quiz
 */

import java.math.*;
public class Chapter3 {

	public static void main(String[] args) {
		System.out.println(hexagonArea(3.16));
		printSpaced("Dr. Iverson");
	}
	
	// calculates the area of a hexagon given one side
	public static double hexagonArea(double side){
		return ((3.0*Math.sqrt(3.0)) / 2.0) * (Math.pow(side, 2));
	}
	
	// prints out string it takes in with spaces in between each character
	public static void printSpaced(String s){
		for(int i = 0; i < s.length(); i++){
			System.out.print(s.charAt(i) + " ");
		}
	}
}