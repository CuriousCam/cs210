/* Camilo Jacomet
 * CS210 - Iverson
 * 1/31/2017
 * Reasons for doing this: Programming assignment #2, Helped learn about how to use graphics properly.
 */

import java.awt.*;

public class BJPgraphic {
	public static final int LINES = 10;
	
	public static void main(String[] args) {
		DrawingPanel p = new DrawingPanel(LINES*10*7,LINES*10*5);
		Graphics g = p.getGraphics();
		printBook(g, LINES*15, LINES);
	}
	
	public static void printBook(Graphics g, int x, int y){		
		int height = LINES*10; // This is the height for the top half of the book
		int width = LINES*30; // The width stays the same throughout all parts of the book
		int bottomBookHeight = LINES*LINES; // This is the height for the bottom half of the book
		double side = (height*(1/(Math.cos(Math.toRadians(30))))); // 
		int side2 = (int)(side);
				
		printTopBook(g,x,y,width,height,side2);
		printBottomBook(g,x-side2,y+height,width,bottomBookHeight);
		printSideBook(g,x+width,y,side2,bottomBookHeight,height);
		
		for(int i = 0; i < LINES; i++){
			for(int j = 0; j < i; j++){
				drawParallelogram(g, x+width-(int)(Math.ceil(side/LINES*i))-j*(width/LINES), y+(height/LINES*i), width/LINES, height/LINES );
				//System.out.println("Width - Ceil: " + (x+width-(int)(Math.ceil(side/LINES*i))) + " Ceil: " + (int)(Math.ceil(side/LINES*i)));
			}
		}
	}
	
	public static void printTopBook(Graphics g, int x, int y, int width, int height, int side){
		g.drawLine(x, y, x+width,y);
		g.drawLine(x+width, y, x+width-side, y+height);
		g.drawLine(x+width-side, y+height, x-side, y+height);
		g.drawLine(x, y, x-side, y+height);
	}
	
	public static void printBottomBook(Graphics g, int x, int y, int width, int height){
		g.drawLine(x, y, x, y+height);
		g.drawLine(x, y+height, x+width, y+height);
		g.drawLine(x+width, y+height, x+width, y);
		printText(g,x,y,width,height);
	}
	
	public static void printText(Graphics g, int x, int y, int width, int height){
		String bjpText = "Building Java Programs";
		for(int i = 0; i < LINES/2; i++){
			g.drawString(bjpText, x+(width-140)/2, y+((i+1)*LINES*2)-i);
		}
	}
	
	
	public static void printSideBook(Graphics g, int x, int y, int width, int bottomBookHeight, int height){
		g.drawLine(x, y, x, y+bottomBookHeight);
		g.drawLine(x, y+bottomBookHeight, x-width, y+bottomBookHeight+(LINES*10));
		for(int i = 0; i < LINES/2; i++){
			g.drawLine(x, y+(i*LINES*2), x-width, y+height+(i*LINES*2));
		}
	}
	
	public static void drawParallelogram(Graphics g, int x, int y,int width, int height){
		int side = (int)Math.ceil((height*(1/(Math.cos(Math.toRadians(30))))));
		g.drawLine(x, y, x-width, y);
		g.drawLine(x-width, y, x-width-side,y+height);
	}
	

}
