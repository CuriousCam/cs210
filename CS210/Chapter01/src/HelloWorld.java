import java.util.*;
public class HelloWorld {

	public static void main(String[] args) {
		Scanner c = new Scanner(System.in);
		inputBirthday(c);
	}
	public static void inputBirthday(Scanner console){
		String month;
	    System.out.print("On what day of the month were you born? ");
	    int day = console.nextInt();
	    System.out.print("What is the name of the month in which you were born? ");
	    month = console.next();
	    System.out.print("During what year were you born? ");
	    int year = console.nextInt();
	    System.out.print("You were born on " + month + " " + day + ", " + year + ". You're mighty old!");
	}

}
