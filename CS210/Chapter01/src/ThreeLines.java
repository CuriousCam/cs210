/* Camilo Jacomet
 * CS210 - Iverson
 * 1/13/2017
 * Reasons for doing this: Chapter 1 Quiz
 */
public class ThreeLines {
	
	// I call the appropriate methods in order to display the ASCII art that the Instructor asked of me.
	public static void main(String[] args) {
		stars();
		methodCallingNameBox();
		stars();
	}
	
	// stars displays the 3 lines of "*" to the screen along with the appropriate spacing.
	public static void stars(){
		System.out.println("   ******************");
		System.out.println("   ******************");
		System.out.println("   ******************");
	}
	
	// nameBox() is used to display the slashes with my name in the middle. 
	public static void nameBox(){
		 System.out.println("  ///////////////////");
	     System.out.println("<|| Camilo Jacomet ||>");
	     System.out.println("  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
	}
	
	// methodCallingNameBox() is used to call nameBox() 3 times due to the requirements of the quiz
	// specifically "3. Each method has only three statements; or less." and "4. You must have a method that calls another method."
	// This also eliminates redundancy of course not having to type out the nameBox() text multiple times.
	public static void methodCallingNameBox(){
		nameBox();
		nameBox();
		nameBox();
	}
}