/* Camilo Jacomet
 * CS210 - Iverson
 * 1/5/2017
 * Reasons for doing this: Because it is Quiz 1.
 */
public class GettingStarted {
	// main method that includes code to display text that quiz asked for.
	public static void main(String[] args) {
        System.out.println("  //////////////////////");
        System.out.println("<||  Camilo Jacomet   ||>");
        System.out.println("  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
	}

}
