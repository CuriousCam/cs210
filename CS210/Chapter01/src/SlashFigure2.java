public class SlashFigure2{
	public static final int LINES = 6;
	
	public static void main(String[] args){
		for(int i = 1; i <= LINES; i++){
			for(int h = 1; h < i*2-1; h++){
				System.out.print('\\');
			}
			for(int j = 0; j < (LINES*4-2)-(i-1)*4; j++){
				System.out.print('!');
			}
			for(int k = 1; k < i*2-1; k++){
				System.out.print('/');
			}
			System.out.println();
		}
		
	}
}