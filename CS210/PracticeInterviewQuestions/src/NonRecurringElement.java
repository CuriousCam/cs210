// The problem: Find the only non recurring element in an array.
import java.util.*;
public class NonRecurringElement {

	public static void main(String[] args) {
		int numbers[] = {1,1,1,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9};
		findSingleElement(numbers);
		System.out.println();
		int numbers2[] = {1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,1,1,1,3,3,4,4,5,5,6,6,7,7,8,8,9,9,21};
		long startTime = System.nanoTime();	
		findSingleElement(numbers2);
		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		System.out.println();
		System.out.print(duration/1000000);
	}
	public static String findSingleElement(int[] arr){
		String value;
		HashSet nonRecurring = new HashSet();
		HashSet recurring = new HashSet();
		for(int i = 0; i < arr.length; i++){
			if(recurring.contains(arr[i])){
			} else if(nonRecurring.contains(arr[i])){
				nonRecurring.remove(arr[i]);
				recurring.add(arr[i]);
			} else 
				nonRecurring.add(arr[i]);
		}
		value = nonRecurring.toString();
		System.out.print(value);
		return value;
	}

}
