import static org.junit.Assert.*;

import org.junit.Test;

public class TestNonRecurringElement extends NonRecurringElement{

	@Test
	public void test() {
		int numbers[] = {1,1,1,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9};
		assertTrue(findSingleElement(numbers).equals("[2]"));
	}

}
