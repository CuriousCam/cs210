/* Author : Camilo Jacomet
 * Assignment : 3
 * Teacher : Taesik Kim
 * Notes : Was unsure whether you by having a "greater" grade you meant strictly greater or if they had the same grade
 * in bothh theyd fall under this category.
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Assignment3 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner fileReader = new Scanner(new File("student.txt"));
		HashSet<String> cs211 = new HashSet<String>(); // Students who took CS210
		HashSet<String> cs210 = new HashSet<String>(); // Students who took CS211
		HashMap<String, Student> Students = new HashMap<String, Student>(); // Map of students with the key value being the student name
		
		// Reading in file and parsing lines
		while(fileReader.hasNextLine()){
			String line = fileReader.nextLine();
			String course = line.substring(0,5);
			String name = line.substring(6, line.indexOf(' ', 6));
			char grade = line.charAt(line.length()-1);
			
			switch(course){
			case "cs210":
				cs210.add(name);
				if(!Students.containsKey(name)){ // If the student is not already in the map
					Students.put(name, new Student(name));
					Students.get(name).setGradeCS210(grade);
				} else
					Students.get(name).setGradeCS210(grade); // If the student is in the map already, just set the grade
				break;
				
			case "cs211":
				cs211.add(name);
				if(!Students.containsKey(name)){ // Same logic as above...
					Students.put(name,  new Student(name));
					Students.get(name).setGradeCS211(grade);
				} else
					Students.get(name).setGradeCS211(grade);
				break;
			}
			
		}
		
		// This prints out all the Students thtat took either cs210 or 211
		HashSet<String> tookEither = new HashSet(cs210);
		tookEither.addAll(cs211);
		System.out.println("Students that took either CS210 OR CS211\n" + printSet(tookEither) + "\n\n");
		
		// This prints out all the Students that took both cs210 and 211
		HashSet<String> tookBoth = new HashSet(cs210);
		tookBoth.retainAll(cs211);
		System.out.println("Students that took both CS210 AND CS211\n" + printSet(tookBoth) + "\n\n");
		
		// This prints out all the Students that took only cs210
		HashSet<String> only210 = new HashSet(cs210);
		only210.removeAll(cs211);
		System.out.println("Students that took only CS210\n" + printSet(only210) + "\n\n");
		
		// This prints out all the students that took only cs211
		HashSet<String> only211 = new HashSet(cs211);
		only211.retainAll(cs210);
		System.out.println("Students that took only CS211\n" + printSet(only211) + "\n\n");
		
		// This prints out the students that took both and received a higher grade in 211
		HashSet<String> tookBothHigherGrade = new HashSet(tookBoth); // need to make copy, but go through tookboth, ran into problem trying to modify set as i iterated through so i made a copy 
		for(String s : tookBoth){
			if(Students.get(s).getGradeCS210() <= Students.get(s).getGradeCS211()){
				tookBothHigherGrade.remove(s);
			}		
		}
		System.out.println("Students that took both CS210 AND CS211, and CS211 grade is higher than CS210 grade\n" + printSet(tookBothHigherGrade) + "\n\n");
		
		// This prints out the students that took both and received the same grade in both
		HashSet<String> tookBothSameGrade = new HashSet(tookBoth);
		for(String s : tookBoth){
			if(Students.get(s).getGradeCS210() != Students.get(s).getGradeCS211()){
				tookBothSameGrade.remove(s);
			}
		}
		System.out.println("Students that took both CS210 AND CS211, and Received same grade in Both\n" + printSet(tookBothSameGrade) + "\n\n");

		

	}
	
	// Method to print set neatly, with fence-post method
	public static String printSet(HashSet<String> course){
		Iterator i = course.iterator();
		int j = 0;
		StringBuilder s = new StringBuilder();
		if(i.hasNext()){
			j++;
			s.append("[ " +i.next());
		}
		while(i.hasNext()){
			j++;
			s.append(", " + i.next());
		}
		s.append(" ]");
		s.insert(0, "Student Count " + j + " : ");
		return s.toString();
	}
}