/* Author : Camilo Jacomet
 * Assignment : Used by Assignment3.java for hashmap
 * Teacher : Taesik Kim
 * 
 */
public class Student {
	private String name;
	private char gradeCS211, gradeCS210;
	
	Student(){
		
	}
	
	Student(String name){
		this.name = name;
	}
	
	void setGradeCS211(char grade){
		this.gradeCS211 = grade;
	}
	
	void setGradeCS210(char grade){
		this.gradeCS210 = grade;
	}
	
	String getName(){
		return this.name;
	}
	
	char getGradeCS211(){
		return this.gradeCS211;
	}
	
	char getGradeCS210(){
		return this.gradeCS210;
	}
}
